/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exePass;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

/**
 *
 * @author Niall
 */
public class purgeDuplicateArray extends RecursiveTask <List<String>> {
    
    private List<String> actList;
    private final Object lock = new Object();
    List<String> cleanPassList = new ArrayList<>();
    
    public purgeDuplicateArray(int s, int e, List<String> pList) {
        List<String> tList = pList.subList(s, e);
        this.actList = tList;
    }
    
    @Override
    protected List<String> compute() {    
        cleanPassList = flagContainStr(actList); 
        System.out.println("size :: " + cleanPassList.size());
        return cleanPassList;
    }
    

    public List<String> flagContainStr(List<String> passList) {
    List<String> retList = new ArrayList<>();
        //synchronized (lock) {
        
            for ( String s : passList) {
                if ( !retList.contains(s)) {
                    retList.add(s);
                }
            }
        //} 
        
        return retList;
    }
    
}
