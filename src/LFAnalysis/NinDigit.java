/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LFAnalysis;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Majorkingpin
 */
public class NinDigit extends RecursiveTask <String[]>{

    private String[] rulel = null;
    private String[] rule2 = null;
    
    private final int idx = 20000;
    String[] passList = new String[idx];
    private final Object lock = new Object();
    
    public NinDigit(String[] constChars, String[] vowChars ) {
        this.rulel = constChars;
        this.rule2 = vowChars;
    }
    
    @Override
    protected String[] compute() { 
        distTable myDist = new distTable();
        Random randomGenerator = new Random();
        
        int ranNumRule = ((4) + 1);
        int actRule = ThreadLocalRandom.current().nextInt(1,4);
        
        int ranNumRule1 = ((23) + 1);
        int run1 = ThreadLocalRandom.current().nextInt(1,23);
        
        int ranNumRule2 = ((66) + 1);
        int run2 = ThreadLocalRandom.current().nextInt(1,66);
        
        int ranNumRule3 = ((20) + 1);
        int run3 = ThreadLocalRandom.current().nextInt(1,20);
        
        int[] distV = myDist.distTable1();
        int[] distMainC = myDist.distTable3();
        
                 for ( int lenNine=0; lenNine < idx; lenNine++ ) {
                    String pass = "";
                        for ( int i = 0; i < 9; i++) {
                            String temp = "";                          
                                actRule = randomGenerator.nextInt(ranNumRule);
                                int n = 0;
                                    if ( actRule == 1 || actRule == 2 || actRule == 6 ) {
                                        run1 = randomGenerator.nextInt(ranNumRule1);
                                        n = distV[run1];
                                        pass = pass + rule2[n];
                                    } else {                                        
                                        run2 = randomGenerator.nextInt(ranNumRule2);
                                        n = distMainC[run2];
                                        pass = pass + rulel[n];
                                    }
                            
                        }
                    
                    boolean opFlag = flagCorrectStr(pass);
                    if ( opFlag == true ) {
                        passList[lenNine] = pass; 
                    }
                    else{
                        if ( lenNine > 0 ) {
                            lenNine--;
                        } else {
                            lenNine = 0;
                        }
                    }
                        
               }
        
        return passList;
    }

 
    
    public boolean flagCorrectStr(String pass) {
        synchronized (lock) {
            boolean tFlag = false;
            
              String testChar = "";
              int vowCT = 0;
              
                tFlag = patternMatch1(rulel,rule2,pass); 
                if ( !tFlag) {
                    tFlag = patternMatch2(rulel,rule2,pass);
                } else if ( !tFlag) {
                    tFlag = patternMatch3(rulel,rule2,pass);
                } 
                
            return tFlag;   
        } 
    }   
    
        public boolean patternMatch1(String[] constant, String[] vowel, String tempStr) {

        List<String> con = (List<String>) Arrays.asList(constant);
        List<String> vow = (List<String>) Arrays.asList(vowel);
        String charc = "";
            
            charc = Character.toString(tempStr.charAt(0));
            if ( !vow.contains(charc)){ return false;} 
            charc = Character.toString(tempStr.charAt(1));
            if ( !con.contains(charc)){ return false;} 
            charc = Character.toString(tempStr.charAt(2));
            if ( !vow.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(3));
            if ( !con.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(4));
            if ( !vow.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(5));
            if ( !con.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(6));
            if ( !vow.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(7));
            if ( !vow.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(8));
            if ( !con.contains(charc)){ return false;}
            
        return true;
    }
    
    public boolean patternMatch2(String[] constant, String[] vowel, String tempStr) {

        List<String> con = (List<String>) Arrays.asList(constant);
        List<String> vow = (List<String>) Arrays.asList(vowel);
        String charc = "";
            
            charc = Character.toString(tempStr.charAt(0));
            if ( !con.contains(charc)){ return false;} 
            charc = Character.toString(tempStr.charAt(1));
            if ( !con.contains(charc)){ return false;} 
            charc = Character.toString(tempStr.charAt(2));
            if ( !vow.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(3));
            if ( !vow.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(4));
            if ( !con.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(5));
            if ( !con.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(6));
            if ( !vow.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(7));
            if ( !con.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(8));
            if ( !con.contains(charc)){ return false;}
            
        return true;
    }
    
    // constanent constanent vowel constanent vowel constanent vowel constanent vowel 
    
    public boolean patternMatch3(String[] constant, String[] vowel, String tempStr) {

        List<String> con = (List<String>) Arrays.asList(constant);
        List<String> vow = (List<String>) Arrays.asList(vowel);
        String charc = "";
            
            charc = Character.toString(tempStr.charAt(0));
            if ( !con.contains(charc)){ return false;} 
            charc = Character.toString(tempStr.charAt(1));
            if ( !con.contains(charc)){ return false;} 
            charc = Character.toString(tempStr.charAt(2));
            if ( !vow.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(3));
            if ( !con.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(4));
            if ( !vow.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(5));
            if ( !con.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(6));
            if ( !vow.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(7));
            if ( !con.contains(charc)){ return false;}
            charc = Character.toString(tempStr.charAt(8));
            if ( !vow.contains(charc)){ return false;}
            
        return true;
    }
    
}