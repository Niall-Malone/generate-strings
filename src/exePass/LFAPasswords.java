/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exePass;
import LFAnalysis.EigDigit;
import LFAnalysis.NinDigit;
import LFAnalysis.SevDigit;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Majorkingpin
 */
public class LFAPasswords {
    
        // 5 Chars
        final static String[] vowChars = {"e","a","o","i","u"};
        
        // 29 Chars
        final static String[] constChars = 
        {
            "r","s","n","t","l","m","d","c","p","h",
            "y","b","k","g","f","w","v","j","x","z","q"
        };
    
        static ForkJoinPool pool = new ForkJoinPool();
        
    public static void main(String[] args) {
        boolean stopFlag = false;
        int len = 0;
        int itr = 0;
        
        System.out.println("Step 1. Starting.......");
        
        SevDigit s3 = new SevDigit(constChars,vowChars);
        SevDigit s2 = new SevDigit(constChars,vowChars);
        SevDigit s1 = new SevDigit(constChars,vowChars);
        SevDigit s4 = new SevDigit(constChars,vowChars);
        EigDigit s5 = new EigDigit(constChars,vowChars);
        EigDigit s6 = new EigDigit(constChars,vowChars);
        EigDigit s7 = new EigDigit(constChars,vowChars);
        EigDigit s8 = new EigDigit(constChars,vowChars);
        NinDigit s9 = new NinDigit(constChars,vowChars);
        NinDigit s10 = new NinDigit(constChars,vowChars);
        NinDigit s11 = new NinDigit(constChars,vowChars);
        NinDigit s12 = new NinDigit(constChars,vowChars);
        
        List<ForkJoinTask> dList = Arrays.asList(new ForkJoinTask[]
        {
            s1,s2,s3,s4,
            s5,s6,s7,s8,
            s9,s10,s11,s12
        });
        
        executePool(dList);
        
        len = dList.size();         
            
         while ( itr < len )  
         {
            System.out.printf("******************************************\n");
            System.out.printf("Main: Parallelism: %d\n", pool.getParallelism());
            System.out.printf("Main: Active Threads: %d\n", pool.getActiveThreadCount());
            System.out.printf("Main: Task Count: %d\n", pool.getQueuedTaskCount());
            System.out.printf("Main: Steal Count: %d\n", pool.getStealCount());
            System.out.printf("******************************************\n");
         
            try
            {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            
            for ( ForkJoinTask f : dList ) {
                if ( f.isDone()) {
                    itr++;
                }
            }
        } 
                       
      pool.shutdown();
      System.out.println("Step 2. Words Generated");          
      List list = returnList(dList);       
      System.out.println("Step 3. List ready to be Cleaned!");
        
        int idx = list.size() / 4;
        int idx2 = idx * 2;
        int idx3 = idx * 3;
        int idx4 = list.size();
        
        purgeDuplicateArray c1 = new purgeDuplicateArray(0,idx,list);
        purgeDuplicateArray c2 = new purgeDuplicateArray(idx,idx2,list);
        purgeDuplicateArray c3 = new purgeDuplicateArray(idx2,idx3,list);
        purgeDuplicateArray c4 = new purgeDuplicateArray(idx2,idx4,list);
        
        pool = new ForkJoinPool();
        List<ForkJoinTask> dList2 = Arrays.asList(new ForkJoinTask[]{c1,c2,c3,c4});
        executePool(dList2);
        
        len = dList2.size();     
        itr = 0;
        
        while ( itr < len ) { 
            try { TimeUnit.SECONDS.sleep(1); 
            } catch (InterruptedException e) { 
                e.printStackTrace();
            }
            
            for ( ForkJoinTask f : dList2 ) {
                if ( f.isDone()) {
                    itr++;
                }
            }
        } 
        
        System.out.println("Step 4. List Cleaned!");
        pool.shutdown();
        List<String> result = returnList2(dList2);
                 
        System.out.println("Step 5. List Combined!");
        writeToFile(result);
        System.out.println("Step 6. File Done!");
   
    }
        
    
    public static void writeToFile(List<String> result) {
        try {
            File file = new File("lfaPasses.lst");
 
			// if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
                
            BufferedWriter out = new BufferedWriter(new FileWriter("lfaPasses.lst"));
            
            for ( Object o: result) {
                if ( o != null ) {
                    String pass = (String)o;
                
                    out.write(pass);
                    out.newLine();
                }                				
            }				
								
						
	     out.close();
            
	   } catch (IOException e) {
               e.printStackTrace();
           }
        
            System.out.println("Finished writing to file!!");
    }
    
    public static void executePool(List<ForkJoinTask> threadList) {
        for (ForkJoinTask o : threadList) {
                pool.execute(o);
        }  
    }
    
    public static List returnList(List<ForkJoinTask> dList) {
        List list = new ArrayList();
        for ( ForkJoinTask f : dList) {
            list.addAll(Arrays.asList((String[]) f.join()));
        }
        return list;
    }
    
    public static List<String> returnList2(List<ForkJoinTask> dList) {
        List<String> result = new ArrayList<String>();
        for ( ForkJoinTask f : dList) {
            result.addAll((Collection<? extends String>) f.join());
        }
        return result;
    }
}

