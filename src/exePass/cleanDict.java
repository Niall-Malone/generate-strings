/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exePass;

import cleanFiles.mergeFiles;
import cleanFiles.opClean;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Majorkingpin
 */
public class cleanDict {
    
    
    public static void main(String[] args) {
        
        System.out.println("Starting to load myDic.lst");

        List myList1 = new ArrayList();
        List myList2 = new ArrayList();
        List myList3 = new ArrayList();
        List myList4 = new ArrayList();
        
        List cleanedList = new ArrayList();
	
        opClean op = new opClean();
        
        myList1 = op.returnArray("C:\\Users\\Majorkingpin\\Documents\\NetBeansProjects\\genPasswords\\tobeCleaned\\lfaPasses.lst");
        myList2 = op.returnArray("C:\\Users\\Majorkingpin\\Documents\\NetBeansProjects\\genPasswords\\tobeCleaned\\lfaPasses2.lst");
        myList3 = op.returnArray("C:\\Users\\Majorkingpin\\Documents\\NetBeansProjects\\genPasswords\\tobeCleaned\\lfaPasses3.lst");
        myList4 = op.returnArray("C:\\Users\\Majorkingpin\\Documents\\NetBeansProjects\\genPasswords\\tobeCleaned\\lfaPasses4.lst");
        
        System.out.println("Dic count is : " + myList1.size());
        System.out.println("Dic2 count is : " + myList2.size());
        System.out.println("Dic3 count is : " + myList3.size());
        System.out.println("Dic4 count is : " + myList4.size());
        
        System.out.println("--------------------------------------------");
        System.out.println("");
        
        mergeFiles m = new mergeFiles();
        cleanedList = m.returnMergedList(myList1, myList2, myList3, myList4);
        
        System.out.println("New list size is " + cleanedList.size());
        
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter("myDic.lst"));
            
            for ( Object o: cleanedList) {
                String pass = (String)o;
            
                
		out.write(pass);
                out.newLine();					
                				
            }				
								
						
	     out.close();
					
	   } catch (IOException e) {
               e.printStackTrace();
           }
        
            System.out.println("Finished writing to file!!");
    }
}
