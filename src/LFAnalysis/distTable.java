/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LFAnalysis;

/**
 *
 * @author Majorkingpin
 */
public class distTable {
    
    // This is the distribution for vowels!!
    // 5 chars
    public int[] distTable1() {
        // 0 = 7 27%
        // 1 = 5  18%
        // 2 = 5  22%
        // 3 = 5   22%
        // 4 = 2   8%
        // 24
        int[] dist = {0,1,3,2,0,1,0,3,2,1,2,3,0,4,1,0,3,0,2,3,1,4,2,0};
        return dist;
    }
    
    // This is the distribution for the champ characters
    // 21 chars
    public int[] distTable2() {
        // 0 =  8     5 =  6        10 =  8         15 =  4       20 = 2 
        // 1 =  6     6 =  8        11 =  4         16 =  8       21 = 2
        // 2 =  8     7 =  6        12 =  4         17 =  4
        // 3 =  8     8 =  6        13 =  4         18 =  2
        // 4 =  8     9 =  4        14 =  4         19 =  2
        // 116
        int[] dist = {17,0,4,10,14,15,12,2,18,9,13,8,5,7,21,16,10,6,2,19,3,4,14,4,5,0,10,0,16,3,6,7,2,8,1,1,9,6,10,6,10,2,3,11,
            4,0,1,12,9,16,2,20,5,3,0,8,13,1,4,7,11,0,16,10,5,3,4,6,12,17,6,1,13,14,3,16,8,15,0,5,11,12,7,2,0,16,17,8,1,18,3,4,0,16,
            3,4,7,2,10,8,17,14,19,5,6,16,6,20,10,9,7,10,15,11,13,21,2,15};        
        return dist;  
    }
    
    // This is the distribution for the main characters
    // 21 chars
    public int[] distTable3() {
        // 0 =  11    5 =  4        10 =  3         15 =  1       20 = 1  
        // 1 =  10     6 =  4        11 =  2         16 =  1               
        // 2 =  8     7 =  4        12 =  2         17 =  1               
        // 3 =  5     8 =  3        13 =  3         18 =  1               
        // 4 =  5     9 =  3        14 =  2         19 =  1              
        
        //66
        int[] dist = {
            0, 8, 11, 5, 0, 1, 2, 0, 0, 1, 2, 0, 5, 0, 
            1, 9, 12, 2, 10, 0, 15, 14, 1, 13, 8, 2, 0, 0, 1, 2, 3, 
            3, 5, 20, 17, 3, 0, 3, 6, 2, 1, 10, 3, 4, 5, 4, 4, 4, 4, 
            6, 9, 2, 6, 1, 13, 14, 8, 6, 7, 16, 7, 7, 7, 9, 1, 10, 11,
            1, 11, 12, 13, 1, 18, 19, 0, 1, 2 
        };        
        return dist;  
    }
}
